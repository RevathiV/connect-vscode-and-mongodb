import express, { application } from 'express';
import bodyParser from "body-parser";
import mongoose from 'mongoose';
import dotenv from 'dotenv'



const app = express()
dotenv.config()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))



const PORT = process.env.PORT || 4000

mongoose.connect(process.env.URL)
.then(()=> app.listen(PORT,()=>console.log('connected')))
.catch((error)=>console.log(error))

